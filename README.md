Aplicativo VipDriver Passageiros
##Installation Guide

- git clone [PROJECT]
- Delete .git folder in Project folder

**Linguagens**

- Java

**added**

- Migração do Android para AndroidX
- Integração com OneSignal (push notification)


**BugFixes**

- Erro no pacote do Android.Support.V4 (AndroidX)

- Erro no BottonSheet Design R.

**Removed**

Atualização do CrashLytics

- Códigos trash

- BugFixes

**Atualização do Gradle**

- Atualização de códigos em geral

- Contribuidores

Victor Ferreira de Lima https://github.com/victorfdelima
