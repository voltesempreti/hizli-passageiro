package com.hizli.user.ui.activity.wallet;

import com.appoets.paytmpayment.PaytmObject;
import com.hizli.user.base.MvpView;
import com.hizli.user.data.network.model.AddWallet;
import com.hizli.user.data.network.model.BrainTreeResponse;

public interface WalletIView extends MvpView {
    void onSuccess(AddWallet object);

    void onSuccess(PaytmObject object);

    void onSuccess(BrainTreeResponse response);
    void onError(Throwable e);
}
