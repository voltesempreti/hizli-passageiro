package com.hizli.user.ui.fragment.searching;

import com.hizli.user.base.MvpView;

public interface SearchingIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);
}
