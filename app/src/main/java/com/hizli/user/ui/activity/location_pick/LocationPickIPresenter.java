package com.hizli.user.ui.activity.location_pick;

import com.hizli.user.base.MvpPresenter;

public interface LocationPickIPresenter<V extends LocationPickIView> extends MvpPresenter<V> {
    void address();
}
