package com.hizli.user.ui.activity.coupon;

import com.hizli.user.base.MvpPresenter;

public interface CouponIPresenter<V extends CouponIView> extends MvpPresenter<V> {
    void coupon();
}
