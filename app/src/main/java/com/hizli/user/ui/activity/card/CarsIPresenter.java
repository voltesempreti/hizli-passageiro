package com.hizli.user.ui.activity.card;

import com.hizli.user.base.MvpPresenter;


public interface CarsIPresenter<V extends CardsIView> extends MvpPresenter<V> {
    void card();
}
