package com.hizli.user.ui.fragment.service;

import com.hizli.user.base.MvpView;
import com.hizli.user.data.network.model.Service;

import java.util.List;

public interface ServiceTypesIView extends MvpView {

    void onSuccess(List<Service> serviceList);

    void onError(Throwable e);

    void onSuccess(Object object);
}
