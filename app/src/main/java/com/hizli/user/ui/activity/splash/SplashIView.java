package com.hizli.user.ui.activity.splash;

import com.hizli.user.base.MvpView;
import com.hizli.user.data.network.model.CheckVersion;
import com.hizli.user.data.network.model.Service;
import com.hizli.user.data.network.model.User;

import java.util.List;

public interface SplashIView extends MvpView {

    void onSuccess(List<Service> serviceList);

    void onSuccess(User user);

    void onError(Throwable e);

    void onSuccess(CheckVersion checkVersion);
}
