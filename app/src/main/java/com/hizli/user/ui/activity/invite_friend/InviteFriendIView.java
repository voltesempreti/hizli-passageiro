package com.hizli.user.ui.activity.invite_friend;

import com.hizli.user.base.MvpView;
import com.hizli.user.data.network.model.User;

public interface InviteFriendIView extends MvpView {

    void onSuccess(User user);

    void onError(Throwable e);

}
