package com.hizli.user.ui.activity.login;

import com.hizli.user.base.MvpView;
import com.hizli.user.data.network.model.ForgotResponse;
import com.hizli.user.data.network.model.Token;

public interface LoginIView extends MvpView {
    void onSuccess(Token token);

    void onSuccess(ForgotResponse object);

    void onError(Throwable e);
}
