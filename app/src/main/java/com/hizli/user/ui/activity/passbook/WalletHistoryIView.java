package com.hizli.user.ui.activity.passbook;

import com.hizli.user.base.MvpView;
import com.hizli.user.data.network.model.WalletResponse;

public interface WalletHistoryIView extends MvpView {
    void onSuccess(WalletResponse response);

    void onError(Throwable e);
}
