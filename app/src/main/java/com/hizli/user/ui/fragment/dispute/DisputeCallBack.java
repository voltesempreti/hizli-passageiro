package com.hizli.user.ui.fragment.dispute;

public interface DisputeCallBack {
    void onDisputeCreated();
}
