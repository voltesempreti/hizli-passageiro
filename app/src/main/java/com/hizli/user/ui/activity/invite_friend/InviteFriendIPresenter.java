package com.hizli.user.ui.activity.invite_friend;

import com.hizli.user.base.MvpPresenter;

public interface InviteFriendIPresenter<V extends InviteFriendIView> extends MvpPresenter<V> {
    void profile();
}
