package com.hizli.user.ui.activity.upcoming_trip_detail;

import com.hizli.user.base.MvpView;
import com.hizli.user.data.network.model.Datum;

import java.util.List;

public interface UpcomingTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> upcomingTripDetails);

    void onError(Throwable e);
}
