package com.hizli.user.ui.activity.help;

import com.hizli.user.base.MvpView;
import com.hizli.user.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help help);

    void onError(Throwable e);
}
