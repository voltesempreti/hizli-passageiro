package com.hizli.user.ui.fragment.book_ride;

import com.hizli.user.base.MvpView;
import com.hizli.user.data.network.model.PromoResponse;


public interface BookRideIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);

    void onSuccessCoupon(PromoResponse promoResponse);
}
