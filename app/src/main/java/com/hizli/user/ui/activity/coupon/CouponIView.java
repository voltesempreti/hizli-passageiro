package com.hizli.user.ui.activity.coupon;

import com.hizli.user.base.MvpView;
import com.hizli.user.data.network.model.PromoResponse;

public interface CouponIView extends MvpView {
    void onSuccess(PromoResponse object);

    void onError(Throwable e);
}
